import React from 'react';
import PropTypes from 'prop-types';
import Locale from '../../locale';

import './message.scss';

const Message = props => {
    const {message, ...otherProps} = props;

    return (
        <div className='error' {...otherProps}>{message}</div>
    );
};

Message.propTypes = {
    message: PropTypes.string
};

Message.defaultProps = {
    label: Locale.error.error
};

export default Message;