import React, { useState } from 'react';
import { questions } from './quizQuestions';
import Button from '../button';
import Locale from '../../locale';

import './quiz.scss'

const Quiz = () => {
    const [currentQuestion, setCurrentQuestion] = useState(0);
    const [showScore, setShowScore] = useState(false);
    const [score, setScore] = useState(0);
    const locale = Locale.quiz;

    const handleAnswerOptionClick = (isCorrect) => {
        if (isCorrect) {
            setScore(score + 1);
        }

        const nextQuestion = currentQuestion + 1;

        if (nextQuestion < questions.length) {
            setCurrentQuestion(nextQuestion);
        }
        else {
            setShowScore(true);
        }
    };

    return (
        <div className='quiz'>
            {showScore ? (
                <div className='score-section'>
                    {locale.sumScore} {score}/{questions.length}!
                </div>
            ) : (
                <>
                    <div className='question-section'>
                        <div className='question-count'>
                            <span>{locale.question} {currentQuestion + 1}</span>/{questions.length}
                        </div>
                        <div className='question-text'>{questions[currentQuestion].questionText}</div>
                    </div>
                    <div className='answer-section'>
                        {questions[currentQuestion].answerOptions.map((answerOption) => (
                            <Button onClick={() => handleAnswerOptionClick(answerOption.isCorrect)} label={answerOption.answerText} key={answerOption.id} />
                        ))}
                    </div>
                </>
            )}
        </div>
    );
}

export default Quiz;