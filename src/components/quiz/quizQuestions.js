export const questions = [
    {
        questionText: 'Начнем с самого простого: кто обезглавил Эддарда Старка?',
        answerOptions: [
            { id: '1', answerText: 'Сир Меррин Трант', isCorrect: false },
            { id: '2', answerText: 'Сир Илин Пейн', isCorrect: true },
            { id: '3', answerText: 'Джоффри Баратеон', isCorrect: false },
            { id: '4', answerText: 'Король Ночи', isCorrect: false },
        ],
    },
    {
        questionText: 'Как звучит девиз дома Ланнистеров?',
        answerOptions: [
            { id: '1', answerText: 'Никто так не жесток, как мы', isCorrect: false },
            { id: '2', answerText: 'Услышь мой рев!', isCorrect: true },
            { id: '3', answerText: 'Ланнистеры всегда платят свои долги', isCorrect: false },
            { id: '4', answerText: 'Первые в битве!', isCorrect: false },
        ],
    },
    {
        questionText: 'Кто прибыл на помощь Джону Сноу в конце серии «За стеной», чтобы спасти его от упырей и отправить к Восточному дозору?',
        answerOptions: [
            { id: '1', answerText: 'Бенджен', isCorrect: true },
            { id: '2', answerText: 'Дейенерис', isCorrect: false },
            { id: '3', answerText: 'Джендри', isCorrect: false },
            { id: '4', answerText: 'Тормунд', isCorrect: false },
        ],
    },
    {
        questionText: 'На Красной свадьбе были жестоко убиты Робб Старк, его жена Талиса, мать Кейтилин и их знаменосцы и солдаты. По какому сигналу на Старков напали?',
        answerOptions: [
            { id: '1', answerText: 'Убийство Талисы ножом в живот', isCorrect: false },
            { id: '2', answerText: 'Подача вина', isCorrect: false },
            { id: '3', answerText: 'Насмешливый крик "Встречайте Короля Севера!"', isCorrect: false },
            { id: '4', answerText: 'Арбалетчики, переодетые в музыкантов, исполнили песню "Дожди в Кастамере"', isCorrect: true },
        ],
    },
    {
        questionText: 'В шестом сезоне Бран узнает, почему дети леса создали Белых ходоков. С какой целью, по словам Листочка, дети леса сделали это?',
        answerOptions: [
            { id: '1', answerText: 'Чтобы победить драконов', isCorrect: false },
            { id: '2', answerText: 'Чтобы защититься от первых людей', isCorrect: true },
            { id: '3', answerText: 'Чтобы защитить Трехглазого ворона', isCorrect: false },
            { id: '4', answerText: 'Чтобы даровать им вечную жизнь', isCorrect: false },
        ],
    },
    {
        questionText: 'Джейме Ланнистер взял Хайгарде в седьмом сезоне благодаря тому, что скопировал тактику Робба Старка во время Битвы…',
        answerOptions: [
            { id: '1', answerText: 'в Шепчущем лесу', isCorrect: true },
            { id: '2', answerText: 'на Желтом Зубце', isCorrect: false },
            { id: '3', answerText: 'у Окскросса', isCorrect: false },
            { id: '4', answerText: 'на Зеленом Зубце', isCorrect: false },
        ],
    },
    {
        questionText: 'Каждому из трех своих драконов Дейенерис дала имя. Какое из нижеперечисленных слов не является именем?',
        answerOptions: [
            { id: '1', answerText: 'Дрогон', isCorrect: false },
            { id: '2', answerText: 'Дракарис', isCorrect: true },
            { id: '3', answerText: 'Визерион', isCorrect: false },
            { id: '4', answerText: 'Рейгаль', isCorrect: false },
        ],
    },
    {
        questionText: 'В третьем сезоне Джейме Ланнистеру отрубают руку. Как зовут персонажа, который сделал это?',
        answerOptions: [
            { id: '1', answerText: 'Лок', isCorrect: true },
            { id: '2', answerText: 'Квиберн', isCorrect: false },
            { id: '3', answerText: 'Сандор Клиган', isCorrect: false },
            { id: '4', answerText: 'Торос из Мира', isCorrect: false },
        ],
    },
    {
        questionText: 'Почему сир Илин Пейн никогда не разговаривает?',
        answerOptions: [
            { id: '1', answerText: 'Он немой', isCorrect: false },
            { id: '2', answerText: 'Он принял обет молчания', isCorrect: false },
            { id: '3', answerText: 'Ему отрезали язык', isCorrect: true },
            { id: '4', answerText: 'Его ранили в горло в одном из сражений', isCorrect: false },
        ],
    }
];