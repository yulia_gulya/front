import React from 'react';
import baseRequest from '../../api';
import Person from '../person';
import PersonForm from '../person-form';
import Message from '../message';
import Button from '../button';
import Navigation from '../navigation';
import Locale from '../../locale';

import './person-list.scss';

class PersonList extends React.Component {
    state = { title: '', person: [], info: '', message: null };

    componentDidMount() {
        baseRequest.get(`${window.location.pathname}`).then(response => {
            if (response.data.status === 'OK') {
                const title = response.data.data.title;
                const person = response.data.data.celebrities;
                const info = response.data.data.info;
                this.setState({ title, person, info });
            }
            else {
                this.setState({ message: response.data.message });
            }
        });
    }

    addPerson = (name, about) => {
        baseRequest
            .post(`${window.location.pathname}`, {
                name,
                about
            })
            .then(response => {
                if (response.data.status === 'OK') {
                    this.setState(prevState => ({
                        person: [...prevState.person, response.data.newPerson]
                    }));
                }
                else {
                    this.setState({ message: response.data.message });
                }
            });
    };

    openModal() {
        document.getElementsByClassName('modal')[0].style.display = 'block';
    }

    render() {
        const { message } = this.state;
        const locale = Locale.main;

        return (
            <div className='page'>
                <Navigation />
                {!message && <Message message={message} /> }
                <h2>{this.state.title}</h2>
                <p>{this.state.info}</p>
                <ul className='person-list'>
                {this.state.person.map(person => (
                    <li className='person' key={person.id}>
                        <Person {...person} />
                    </li>
                ))}
                </ul>
                <Button onClick={this.openModal} label={locale.addPerson} />
                <div className='modal'>
                    <PersonForm addPerson={this.addPerson} />
                </div>
            </div>
        );
    }
}

export default PersonList;