import React from 'react';
import PropTypes from 'prop-types';

import './button.scss';

const Button = props => {
    const { label, ...otherProps } = props;

    return (
        <div className='container-btn'>
            <a className='button' {...otherProps}>
                <span className='link-content'>{label}</span>
            </a>
        </div>
    );
};

Button.propTypes = {
    label: PropTypes.string
};

Button.defaultProps = {
    label: 'SUBMIT'
};

export default Button;