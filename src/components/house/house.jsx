import React from 'react';
import { Link } from 'react-router-dom';
import Locale from '../../locale';

import './house.scss';

const locale = Locale.main;

const House = props => {
    return (
        <div className='house'>
            <h2>{props.title}</h2>
            <Link to={`/${props.url}`}>
                <img src={props.emblem} alt='' />
            </Link>
            <ul>
                <li><span>{locale.land} - </span>{props.land}</li>
                <li><span>{locale.castle} - </span>{props.castle}</li>
                <li><span>{locale.head} - </span>{props.head}</li>
            </ul>
        </div>
    )
};

export default House;