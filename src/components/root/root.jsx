import React from 'react';
import { Route, Switch } from 'react-router-dom';
import HousesList from '../houses-list';
import PersonList from '../person-list';
import PersonInfo from '../person-info';

import './root.scss';

const Root = () => {
    return (
        <Switch>
            <Route exact path='/' component={HousesList} />
            <Route exact path='/:url' component={PersonList} />
            <Route path='/:url/:id' component={PersonInfo} />
        </Switch>
    );
};

export default Root;