import React from 'react';
import { Link } from 'react-router-dom';
import Locale from '../../locale';

import './navigation.scss';

class Navigation extends React.Component {

    render() {
        const locale = Locale.button;

        return (
            <Link className='arrow' to={window.location.pathname.split('/')[0]}>
                <svg className='arrow-icon' xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
                    <g fill="none" stroke="#66fcf1" strokeWidth="1.5" strokeLinejoin="round" strokeMiterlimit="10">
                        <path className='arrow-circle-icon--arrow' d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"/>
                    </g>
                </svg>
                {locale.back}
            </Link>
        )
    }
}

export default Navigation;