import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

import './pesron.scss';

const Person = props => {
      return (
          <span onClick={() => props.history.push(`${window.location.pathname}/${props.id}`)}>{props.name}</span>
        )
};

Person.propTypes = {
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    about: PropTypes.string
}

export default withRouter(Person);