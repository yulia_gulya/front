import React from 'react';
import PropTypes from 'prop-types';
import Button from '../button';
import Locale from '../../locale';

import './person-form.scss';

class PersonForm extends React.Component {

    state = { name: '', about: '' };

    static propTypes = {
        addPerson: PropTypes.func.isRequired
    };

    handleInputName = event => {
        this.setState({ name: event.target.value })
        document.getElementsByClassName('required')[0].style.display = 'none';
    }

    handleInputAbout = event => {
        this.setState({ about: event.target.value })
    }

    addPerson = () => {
        const { addPerson } = this.props;
        const { name, about } = this.state;

        if (name !== '') {
            addPerson(name, about);
            this.closeModal();
        }
        else {
            document.getElementsByClassName('required')[0].style.display = 'block';
        }
    }

    closeModal() {
        document.getElementsByClassName('modal')[0].style.display = 'none';
    }

    render() {
        const { name, about } = this.state;

        const localeMain = Locale.main;
        const localeError = Locale.error;
        const localeButton = Locale.button;
        const localePlaceholder = Locale.placeholder;

        return (
            <div className='form'>
                <h2 className='title'>{localeMain.addPerson}</h2>
                <span onClick={this.closeModal} className='close-btn'>
                    <img width='32px' src="https://cdn4.iconfinder.com/data/icons/miu/22/circle_close_delete_-128.png" alt='Закрыть' />
                </span>
                <div className='inputs'>
                    <input type='text' placeholder={localePlaceholder.name} value={name} onChange={this.handleInputName} />
                    <span className='required'>{localeError.required}</span>
                    <textarea placeholder={localePlaceholder.about} value={about} onChange={this.handleInputAbout} />
                </div>
                <Button onClick={this.addPerson} label={localeButton.add} />
            </div>
        );
    }
}

export default PersonForm;