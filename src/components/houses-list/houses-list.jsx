import React from 'react';
import baseRequest from '../../api';
import House from '../house';
import Quiz from '../quiz';
import Button from '../button';
import Locale from '../../locale';

import './houses-list.scss';

class HousesList extends React.Component {
    state = { houses: [] };

    componentDidMount() {
        baseRequest.get('/').then(response => {
            const houses = response.data.data;
            this.setState({ houses });
        });
    }

    showQuiz() {
        document.getElementsByClassName('container-btn')[0].style.display = 'none';
        document.getElementsByClassName('quiz')[0].style.display = 'block';
    }

    render() {
        const locale = Locale.button;

        return (
            <div className='houses'>
                {this.state.houses.map(house => (
                    <House {...house} key={house.id} />
                ))}
                <Button onClick={this.showQuiz} label={locale.quiz} />
                <Quiz />
            </div>
        );
    }
}

export default HousesList;