import React from 'react';
import baseRequest from '../../api';
import Button from '../button';
import Navigation from '../navigation';
import Locale from '../../locale';
import Message from '../message';

import './person-info.scss';

class PersonInfo extends React.Component {
    state = {name: '', about: '', message: null};

    componentDidMount() {
        baseRequest.get(`${window.location.pathname}`).then(response => {
            if (response.data.data) {
                this.setState({
                    name: response.data.data.name,
                    about: response.data.data.about
                });
            }
            else {
                this.setState({ message: response.data.message });
            }
        });
    };

    deletePerson = () => {
        baseRequest
            .delete(`${window.location.pathname}`)
            .then(response => {
                if (response.data.status === 'OK') {
                    this.setState({
                        name: '',
                        about: ''
                    });
                    window.history.back();
                }
                else {
                    this.setState({ message: response.data.message });
                }
            });
    };

    render() {
        const locale = Locale.button;
        const { message } = this.state;

        return (
            <div className='page'>
                <Navigation />
                <h2>{this.state.name}</h2>
                <p className='about'>{this.state.about}</p>
                {!message && <Button onClick={this.deletePerson} label={locale.deletePerson} />}
                {message && <Message message={message} /> }
            </div>
        )
    }
}

export default PersonInfo;