// eslint-disable-next-line import/no-anonymous-default-export
export default {
    main: {
        land: 'Земли',
        castle: 'Замок',
        head: 'Глава',
        addPerson: 'Добавить персонажа',
    },
    error: {
        error: 'Что-то пошло не так',
        required: 'Обязательное поле!'
    },
    button: {
        back: 'На главную',
        add: 'Добавить',
        deletePerson: 'Удалить персонажа',
        quiz: 'Пройти тест'
    },
    placeholder: {
        name: 'Имя',
        about: 'Описание'
    },
    quiz: {
        sumScore: 'Вы набрали',
        question: 'Вопрос'
    }
}